﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class play_audioBook : MonoBehaviour {

	public AudioClip BookClip;
	public AudioSource BookSource;
    private static play_audioBook LastClickSound;

	// Use this for initialization
	void Start ()
    {
       
	}
	
	// Update is called once per frame
	void Update () { 

	}

	void OnMouseDown() {
        //BookSource.Play();
        if (LastClickSound != null && LastClickSound.BookSource != null)
        {
            LastClickSound.BookSource.Stop();
        }
        BookSource.Play();
        LastClickSound = this;
    }

}

    //void PlaySound()
    //{
    /* if (LastClickSound != null && LastClickSound.BookSource != null)
     {
         LastClickSound.BookSource.Stop();
     }
     BookSource.Play();
     LastClickSound = this;
    */
    //}

